/*
Copyright (c) 2017 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class LinksClass {
	public var selfProperty : SelfClass?
	public var html : Html?
	public var issue : Issue?
	public var comments : Comments?
	public var review_comments : Review_comments?
	public var review_comment : Review_comment?
	public var commits : Commits?
	public var statuses : Statuses?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let _links_list = _links.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of _links Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [LinksClass]
    {
        var models:[LinksClass] = []
        for item in array
        {
            models.append(LinksClass(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let _links = _links(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: _links Instance.
*/
	required public init?(dictionary: NSDictionary) {

		if (dictionary["self"] != nil) { selfProperty = SelfClass(dictionary: dictionary["self"] as! NSDictionary) }
		if (dictionary["html"] != nil) { html = Html(dictionary: dictionary["html"] as! NSDictionary) }
		if (dictionary["issue"] != nil) { issue = Issue(dictionary: dictionary["issue"] as! NSDictionary) }
		if (dictionary["comments"] != nil) { comments = Comments(dictionary: dictionary["comments"] as! NSDictionary) }
		if (dictionary["review_comments"] != nil) { review_comments = Review_comments(dictionary: dictionary["review_comments"] as! NSDictionary) }
		if (dictionary["review_comment"] != nil) { review_comment = Review_comment(dictionary: dictionary["review_comment"] as! NSDictionary) }
		if (dictionary["commits"] != nil) { commits = Commits(dictionary: dictionary["commits"] as! NSDictionary) }
		if (dictionary["statuses"] != nil) { statuses = Statuses(dictionary: dictionary["statuses"] as! NSDictionary) }
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.selfProperty?.dictionaryRepresentation(), forKey: "self")
		dictionary.setValue(self.html?.dictionaryRepresentation(), forKey: "html")
		dictionary.setValue(self.issue?.dictionaryRepresentation(), forKey: "issue")
		dictionary.setValue(self.comments?.dictionaryRepresentation(), forKey: "comments")
		dictionary.setValue(self.review_comments?.dictionaryRepresentation(), forKey: "review_comments")
		dictionary.setValue(self.review_comment?.dictionaryRepresentation(), forKey: "review_comment")
		dictionary.setValue(self.commits?.dictionaryRepresentation(), forKey: "commits")
		dictionary.setValue(self.statuses?.dictionaryRepresentation(), forKey: "statuses")

		return dictionary
	}

}
