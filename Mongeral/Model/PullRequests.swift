/*
Copyright (c) 2017 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class PullRequests {
	public var url : String?
	public var id : Int?
	public var html_url : String?
	public var diff_url : String?
	public var patch_url : String?
	public var issue_url : String?
	public var number : Int?
	public var state : String?
	public var locked : String?
	public var title : String?
	public var user : User?
	public var body : String?
	public var created_at : String?
	public var updated_at : String?
	public var closed_at : String?
	public var merged_at : String?
	public var merge_commit_sha : String?
	public var assignee : String?
	public var assignees : Array<String>?
	public var milestone : String?
	public var commits_url : String?
	public var review_comments_url : String?
	public var review_comment_url : String?
	public var comments_url : String?
	public var statuses_url : String?
	public var head : Head?
	public var base : Base?
	public var _links : LinksClass?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let json4Swift_Base_list = Json4Swift_Base.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Json4Swift_Base Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [PullRequests]
    {
        var models:[PullRequests] = []
        for item in array
        {
            models.append(PullRequests(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let json4Swift_Base = Json4Swift_Base(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Json4Swift_Base Instance.
*/
	required public init?(dictionary: NSDictionary) {

		url = dictionary["url"] as? String
		id = dictionary["id"] as? Int
		html_url = dictionary["html_url"] as? String
		diff_url = dictionary["diff_url"] as? String
		patch_url = dictionary["patch_url"] as? String
		issue_url = dictionary["issue_url"] as? String
		number = dictionary["number"] as? Int
		state = dictionary["state"] as? String
		locked = dictionary["locked"] as? String
		title = dictionary["title"] as? String
		if (dictionary["user"] != nil) { user = User(dictionary: dictionary["user"] as! NSDictionary) }
		body = dictionary["body"] as? String
		created_at = dictionary["created_at"] as? String
		updated_at = dictionary["updated_at"] as? String
		closed_at = dictionary["closed_at"] as? String
		merged_at = dictionary["merged_at"] as? String
		merge_commit_sha = dictionary["merge_commit_sha"] as? String
		assignee = dictionary["assignee"] as? String
//		if (dictionary["assignees"] != nil) { assignees = assignees.modelsFromDictionaryArray(dictionary["assignees"] as! NSArray) }
		milestone = dictionary["milestone"] as? String
		commits_url = dictionary["commits_url"] as? String
		review_comments_url = dictionary["review_comments_url"] as? String
		review_comment_url = dictionary["review_comment_url"] as? String
		comments_url = dictionary["comments_url"] as? String
		statuses_url = dictionary["statuses_url"] as? String
		if (dictionary["head"] != nil) { head = Head(dictionary: dictionary["head"] as! NSDictionary) }
		if (dictionary["base"] != nil) { base = Base(dictionary: dictionary["base"] as! NSDictionary) }
		if (dictionary["_links"] != nil) { _links = LinksClass(dictionary: dictionary["_links"] as! NSDictionary) }
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.url, forKey: "url")
		dictionary.setValue(self.id, forKey: "id")
		dictionary.setValue(self.html_url, forKey: "html_url")
		dictionary.setValue(self.diff_url, forKey: "diff_url")
		dictionary.setValue(self.patch_url, forKey: "patch_url")
		dictionary.setValue(self.issue_url, forKey: "issue_url")
		dictionary.setValue(self.number, forKey: "number")
		dictionary.setValue(self.state, forKey: "state")
		dictionary.setValue(self.locked, forKey: "locked")
		dictionary.setValue(self.title, forKey: "title")
		dictionary.setValue(self.user?.dictionaryRepresentation(), forKey: "user")
		dictionary.setValue(self.body, forKey: "body")
		dictionary.setValue(self.created_at, forKey: "created_at")
		dictionary.setValue(self.updated_at, forKey: "updated_at")
		dictionary.setValue(self.closed_at, forKey: "closed_at")
		dictionary.setValue(self.merged_at, forKey: "merged_at")
		dictionary.setValue(self.merge_commit_sha, forKey: "merge_commit_sha")
		dictionary.setValue(self.assignee, forKey: "assignee")
		dictionary.setValue(self.milestone, forKey: "milestone")
		dictionary.setValue(self.commits_url, forKey: "commits_url")
		dictionary.setValue(self.review_comments_url, forKey: "review_comments_url")
		dictionary.setValue(self.review_comment_url, forKey: "review_comment_url")
		dictionary.setValue(self.comments_url, forKey: "comments_url")
		dictionary.setValue(self.statuses_url, forKey: "statuses_url")
		dictionary.setValue(self.head?.dictionaryRepresentation(), forKey: "head")
		dictionary.setValue(self.base?.dictionaryRepresentation(), forKey: "base")
		dictionary.setValue(self._links?.dictionaryRepresentation(), forKey: "_links")

		return dictionary
	}

}
