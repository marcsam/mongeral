//
//  RepositoriesTableViewController.swift
//  Mongeral
//
//  Created by Marcelo Sampaio on 2/25/17.
//  Copyright © 2017 Marcelo Sampaio. All rights reserved.
//

import UIKit
import SystemConfiguration
import CoreData


// MARK: - Constants
let MAIN_SERVICE_URL  = "https://api.github.com/search/repositories?q=language:Swift&sort=stars&page="
let ROW_HEIGHT = 191
var reachability = Reachability(hostname: "www.apple.com")


// MARK: - Core Data Specs
var context:NSManagedObjectContext = (UIApplication.shared.delegate as! AppDelegate).managedObjectContext
var objectList = [NSManagedObject]()


class RepositoriesTableViewController: UITableViewController {

    // Core Data Managed Object Context
    var managedObjectContext : NSManagedObjectContext? = (UIApplication.shared.delegate as? AppDelegate)?.managedObjectContext
    
    
    
    // MARK: - Outlets
    
    @IBOutlet var initialImage: UIImageView!
    
    // MARK: - Properties
    private var source = [Items]()
    private var page = 1
    private var alamoCalled = false
    private var cache:NSCache<AnyObject, AnyObject>!
    private var CDRepositoriesArray = [CDRepositories]()

    
    // MARK: - View Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        self.cache = NSCache()
        self.tableView.estimatedRowHeight = CGFloat(ROW_HEIGHT)
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.initialImage.isHidden = false
        
        // reachability.currentReachabilityStatus
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(reachabilityStatusChanged(_:)),
                                               name: NSNotification.Name(rawValue: "ReachabilityChangedNotification"),
                                               object: nil)

        
        // check internet status
        checkInternetStatus()

    
    }
    

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if reachability.isReachable() {
            return source.count
        }else{
            return CDRepositoriesArray.count
        }
        
        
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(
            withIdentifier: "Cell",
            for: indexPath) as! RepositoryTableViewCell

        // Internet Connection is OK
        if reachability.isReachable() {
        
            let item = source[indexPath.row]
            
            cell.repositoryName.text = item.name
            cell.ownerName.text = item.owner?.login
            cell.repositoryDescription.text = item.description
            cell.starsLabel.text = String(describing: item.stargazers_count!) + " Stars"
            cell.forksLabel.text = String(describing: item.forks_count!) + " Forks"
            
            // owner image
            if (self.cache.object(forKey: (indexPath as NSIndexPath).row as AnyObject) != nil){
                cell.ownerImage.image = self.cache.object(forKey: (indexPath as NSIndexPath).row as AnyObject) as? UIImage
            }else{
                let url = URL(string: (item.owner?.avatar_url)!)
                getDataFromUrl(url: url!) { (data, response, error)  in
                    guard let data = data, error == nil else { return }
                    
                    DispatchQueue.main.async() { () -> Void in
                        cell.ownerImage.image = UIImage(data: data)
                        self.cache.setObject(cell.ownerImage.image!, forKey: (indexPath as NSIndexPath).row as AnyObject)
                    }
                }
            }
            return cell
        } else {
            
            // Internet is not available then get data from Core Data
            let item = self.CDRepositoriesArray[indexPath.row]
            cell.repositoryName.text = item.repoTitle
            cell.ownerName.text = item.repoUserName
            cell.repositoryDescription.text = item.repoDescription
            cell.starsLabel.text = String(describing: item.repoStarsQty!) + " Stars"
            cell.forksLabel.text = String(describing: item.repoForksQty!) + " Forks"
            cell.ownerImage.image = UIImage(named: "githubLogo")
            return cell
        }
        

        
    }

    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if !reachability.isReachable() {
            self.tableView.deselectRow(at: indexPath, animated: true)
            alert(msg: "É necessário acesso à Internet para acessar 'Pull Requests'.")
            return
        }
        
        self.performSegue(withIdentifier: "showPullRequests", sender: self)
    }

    
    
    // MARK: - Alamofire
    private func callAlamo(){
        
        let url = MAIN_SERVICE_URL + String(page)
        // activity indicator
        self.activityIndicator(show: true)
        
        
        // Clean-up Core Data if it is needed
        if self.source.count == 0 {
            ServiceCoreData.removeRepositories()
        }
        
        self.CDRepositoriesArray.removeAll()
        
        ServiceAPI.getRepositories(url, success: {
            (itemsArray) -> Void in

            // get api results
            self.source.append(contentsOf: itemsArray)

            
            self.alamoCalled = false
            
            if self.source.count > 0 {
                self.initialImage.isHidden = true
                self.initialImage.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
            }
            
            // Core Data Persistance
            self.CDRepositoriesArray.removeAll()
            self.CDRepositoriesArray = ServiceCoreData.addRepository(repoArray: self.source, inManagedObjectContext: context)
            
            // reload table view
            self.tableView.reloadData()
            
            // activity indicator
            self.activityIndicator(show: false)

            
        }) {
            (error) -> Void in
            print(error)
        }
    }
    
    // MARK: - UIScrollView Delegate - Endless Pagination
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let offsetY = scrollView.contentOffset.y
        let bounds = scrollView.bounds;
        let size = scrollView.contentSize;
        let inset = scrollView.contentInset;
        let y = offsetY + bounds.size.height - inset.bottom;
        let h = size.height;
        
        // call your API for more data
        if !alamoCalled && Int(y) == Int(h) && y > 100 && reachability.isReachable() {
            alamoCalled = true
            page = page + 1
            callAlamo()
        }

        
    }

    
    
    // MARK: - Activity Indicator
    private func activityIndicator(show: Bool) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = show
    }
    
    // MARK: - Image Helper
    private func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
    
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let indexPath = self.tableView.indexPathForSelectedRow
        
        if segue.identifier == "showPullRequests" {
            let pullRequests = segue.destination as! PullRequestsTableViewController
            
            let item = source[(indexPath?.row)!]
            pullRequests.repositoryTitle = item.name!
            pullRequests.urlParam = item.pulls_url!

        }
    }

    // MARK: - Reachability
    // rawValue: ReachabilityChangedNotification)
    @objc private func reachabilityStatusChanged(_ sender: NSNotification) {

        guard ((sender.object as? Reachability)?.currentReachabilityStatus) != nil else { return }
   
        checkInternetStatus()

    }

    private func checkInternetStatus() {
        
        if !reachability.isReachable() {
            navigationController?.navigationBar.barTintColor = UIColor.red
            self.navigationItem.prompt = "Sem conexão com a Internet"

            getRepositoriesFromCoreData()
        }else{
            navigationController?.navigationBar.barTintColor = UIColor.white
            self.navigationItem.prompt = nil
            self.source = [Items]()
            self.page = 1
            self.cache.removeAllObjects()
            
//            tableView.layoutIfNeeded()
//            tableView.contentOffset = CGPoint(x: 0, y: tableView.contentInset.top)
            
            callAlamo()
        }

    }
    
    
    
    
    // MARK: - Core Data
    private func getRepositoriesFromCoreData () {
        
        CDRepositoriesArray = [CDRepositories]()
        self.cache.removeAllObjects()
        
        ServiceCoreData.getRepositories(success: {
            (CoreDataRepositories) -> Void in
            
            self.CDRepositoriesArray = CoreDataRepositories
            if self.CDRepositoriesArray.count > 0 {
                self.initialImage.isHidden = true
                self.initialImage.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
                
                // reload table view
                self.tableView.reloadData()
                
                // activity indicator
                self.activityIndicator(show: false)

            }
            
        }) {
            (error) -> Void in
            print(error)
            return
        }
        


    }

    // MARK: - Application Helper
    private func alert(msg: String) {
        
        let alertController = UIAlertController(title: msg, message: nil, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .cancel) { (action) in
        }
        alertController.addAction(okAction)
        
        self.present(alertController, animated: true) {
        }
        
    }
    
    
    
    
}
