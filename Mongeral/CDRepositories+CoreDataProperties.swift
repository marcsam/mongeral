//
//  CDRepositories+CoreDataProperties.swift
//  PHCoredata
//
//  Created by Marcelo Sampaio on 3/4/17.
//  Copyright © 2017 Marcelo Sampaio. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension CDRepositories {

    @NSManaged var repoTitle: String?
    @NSManaged var repoUserImageUrl: String?
    @NSManaged var repoUserImageBin: Data?
    @NSManaged var repoUserName: String?
    @NSManaged var repoDescription: String?
    @NSManaged var repoStarsQty: NSNumber?
    @NSManaged var repoForksQty: NSNumber?
    @NSManaged var popularRepositories: NSSet?

}
