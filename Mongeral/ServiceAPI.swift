//
//  ServiceAPI.swift
//  Mongeral
//
//  Created by Marcelo Sampaio on 2/27/17.
//  Copyright © 2017 Marcelo Sampaio. All rights reserved.
//

import UIKit
import Alamofire

class ServiceAPI : NSObject {

    // MARK: - Repositories
    class func getRepositories(_ strURL: String, success:@escaping ([Items]) -> Void, failure:@escaping (Error) -> Void) {
        var repositoriesArray = [Items]()
        
        Alamofire.request(strURL).responseJSON { (response) -> Void in
            if response.result.isSuccess {
                if let result = response.result.value {
                    let JSON = result as! NSDictionary
                    let repositories = Repositories.init(dictionary: JSON)
                    
                    for item in (repositories?.items)! {
                        // add item to source list
                        repositoriesArray.append(item)
                    }
                }
                success(repositoriesArray)
            }
            if response.result.isFailure {
                let error : Error = response.result.error!
                failure(error)
            }
        }
    }
    
    // MARK: - Pull Requests
    class func getPullRequests(_ strURL: String, success:@escaping ([PullRequests]) -> Void, failure:@escaping (Error) -> Void) {
        var pullRequestsArray = [PullRequests]()
        
        Alamofire.request(strURL).responseJSON { (response) -> Void in
            if response.result.isSuccess {
                if let result = response.result.value {
                    let resultArray = result as! NSArray
                    
                    
                    for item in resultArray {
                        let JSON = item as! NSDictionary
                        let pullRequest = PullRequests(dictionary: JSON)
                        
                        pullRequestsArray.append(pullRequest!)
                    }
                }
                success(pullRequestsArray)
            }
            if response.result.isFailure {
                let error : Error = response.result.error!
                failure(error)
            }
        }
    }

    
}




