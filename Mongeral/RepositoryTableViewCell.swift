//
//  RepositoryTableViewCell.swift
//  Mongeral
//
//  Created by Marcelo Sampaio on 2/26/17.
//  Copyright © 2017 Marcelo Sampaio. All rights reserved.
//

import UIKit

class RepositoryTableViewCell: UITableViewCell {
    
    
    
    @IBOutlet var repositoryName: UILabel!
    @IBOutlet var ownerImage: UIImageView!
    @IBOutlet var ownerName: UILabel!
    @IBOutlet var repositoryDescription: UILabel!
    @IBOutlet var starsImage: UIImageView!
    @IBOutlet var starsLabel: UILabel!
    @IBOutlet var forksImage: UIImageView!
    @IBOutlet var forksLabel: UILabel!
    
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
