//
//  PullRequestsTableViewController.swift
//  Mongeral
//
//  Created by Marcelo Sampaio on 2/26/17.
//  Copyright © 2017 Marcelo Sampaio. All rights reserved.
//

import UIKit


class PullRequestsTableViewController: UITableViewController {

    // MARK: - Outlets
    
    @IBOutlet var initialMessage: UILabel!
    
    // MARK: - Properties
    var repositoryTitle = String()
    var urlParam = String()
    var url = String()
    var source = [PullRequests]()
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.estimatedRowHeight = CGFloat(ROW_HEIGHT)
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        // clean-up url parameter
        url = urlParam.replacingOccurrences(of: "{/number}", with: "")
        
        self.navigationItem.title = repositoryTitle
        self.navigationItem.prompt = "Pull Requests"
        
        initialMessage.isHidden = false
        
        // get pull requests
        callAlamo()
        
    }

    
    // MARK: - Alamofire
    private func callAlamo(){
        
        
        // activity indicator
        self.activityIndicator(show: true)

        
        ServiceAPI.getPullRequests(url, success: {
            (pullRequestsArray) -> Void in
            
            // get api results
            self.source.append(contentsOf: pullRequestsArray)
            
            if self.source.count > 0 {
                self.initialMessage.isHidden = true
                self.initialMessage.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
            }
            
            // reload table view
            self.tableView.reloadData()
            
            // activity indicator
            self.activityIndicator(show: false)
            
            
        }) {
            (error) -> Void in
            print(error)
        }

        
    }

    
    // MARK: - TableView Data Source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return source.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: "Cell", for: indexPath) as! PullRequestTableViewCell
        
        let pullRequest = source[indexPath.row] 
        
        cell.pullRequestName.text = pullRequest.user?.login
        cell.pullRequestTitle.text = pullRequest.title
        cell.pullRequestBody.text = pullRequest.body
        cell.pullRequestDate.text = self.getFormattedDate(dateString: pullRequest.created_at!)
        
        
        // User Image
        let url = URL(string: (pullRequest.user?.avatar_url)!)
        getDataFromUrl(url: url!) { (data, response, error)  in
            guard let data = data, error == nil else { return }

            DispatchQueue.main.async() { () -> Void in
                cell.pullRequestUserImage.image = UIImage(data: data)
            }
        }

        
        return cell
        
        
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        let pullRequest = source[indexPath.row]
        let url = URL(string: pullRequest.html_url!)
        UIApplication.shared.openURL(url!)
        
    }
    

    
    // MARK: - Activity Indicator
    private func activityIndicator(show: Bool) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = show
    }
    
    
    // MARK: - Image Helper
    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
    
    // MARK: - Source Helper
    private func getFormattedDate(dateString: String) -> String {
        let dateTimeArray = dateString.components(separatedBy: "T")
        let dateString = dateTimeArray[0]
        let dateArray = dateString.components(separatedBy: "-")
        let timeString = dateTimeArray[1]
        let timeArray = timeString.components(separatedBy: ":")
        return dateArray[2] + "/" + dateArray[1] + "/" + dateArray[0] + " " + timeArray[0] + ":" + timeArray[1]
    }
    
    
}
