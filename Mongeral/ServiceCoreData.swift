//
//  ServiceCoreData.swift
//  Mongeral
//
//  Created by Marcelo Sampaio on 3/4/17.
//  Copyright © 2017 Marcelo Sampaio. All rights reserved.
//

import UIKit
import CoreData

class ServiceCoreData: NSObject {

    // MARK: - Add Repository
    class func addRepository(repoArray : [Items], inManagedObjectContext context : NSManagedObjectContext) -> [CDRepositories]{
        
        var coreDataArray = [CDRepositories]()
        
        var i = 0
        for item in repoArray {
            
            let repo = NSEntityDescription.insertNewObject(forEntityName: "CDRepositories", into: context) as! CDRepositories
            
            repo.repoTitle = item.name
            repo.repoUserName = item.owner?.login
            repo.repoDescription = item.description
            repo.repoUserImageUrl = item.owner?.avatar_url
            repo.repoStarsQty = item.stargazers_count as NSNumber?
            repo.repoForksQty = item.forks_count! as NSNumber?
            
            coreDataArray.append(repo)
            i = i + 1
            do {
                try context.save()
            } catch {
            }
        }
    
        return coreDataArray
    }
    

    // MARK: - Get Repositories
    class func getRepositories(success:@escaping ([CDRepositories]) -> Void, failure:@escaping (Error) -> Void) {
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "CDRepositories")
        request.returnsObjectsAsFaults = false
        
        var resultRepoArray = [CDRepositories]()
        
        do {
            let results = try context.fetch(request)
            
            
            for item in results {
                let repo = item as! CDRepositories
                resultRepoArray.append(repo)
            }
            success(resultRepoArray)
            
        } catch let error {
            failure(error)
        }
        
    }
    
    
    // MARK: - Remove Repositories
    class func removeRepositories() {
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "CDRepositories")
        request.returnsObjectsAsFaults = false
        
        do {
            let results = try context.fetch(request)
            
            
            for item in results {
                let repo = item as! CDRepositories
                context.delete(repo)
            }
            do {
                try context.save()
            } catch {
                
            }
        
        } catch {
        }
    }
    
}
