//
//  CDPullRequests+CoreDataProperties.swift
//  PHCoredata
//
//  Created by Marcelo Sampaio on 3/4/17.
//  Copyright © 2017 Marcelo Sampaio. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension CDPullRequests {

    @NSManaged var prTitle: String?
    @NSManaged var prUserImageUrl: String?
    @NSManaged var prUserImageBin: Data?
    @NSManaged var prUserName: String?
    @NSManaged var prDateTime: String?
    @NSManaged var prBody: String?
    @NSManaged var prHtmlUrl: String?
    @NSManaged var repoPullRequests: CDRepositories?

}
