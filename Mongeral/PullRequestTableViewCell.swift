//
//  PullRequestTableViewCell.swift
//  Mongeral
//
//  Created by Marcelo Sampaio on 2/26/17.
//  Copyright © 2017 Marcelo Sampaio. All rights reserved.
//

import UIKit

class PullRequestTableViewCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet var pullRequestTitle: UILabel!
    @IBOutlet var pullRequestBody: UILabel!
    @IBOutlet var pullRequestName: UILabel!
    @IBOutlet var pullRequestUserImage: UIImageView!
    @IBOutlet var pullRequestDate: UILabel!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
